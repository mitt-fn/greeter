#!/usr/bin/python3

class Greeter:
    def __init__(self, name, gender=None):
        self.name = name
        if gender == "male":
            self.gender_greeting = "Herr"
        elif gender == "female":
            self.gender_greeting = "Frau"
        else:
            self.gender_greeting = None

    def greet(self):
        if self.gender_greeting is not None:
            return "Hallo, " + self.gender_greeting + " " + self.name + "!"
        else:
            return "Hallo, " + self.name + "!"


