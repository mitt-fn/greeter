#!/usr/bin/python3

import unittest
from greeter import Greeter

class testGreeter(unittest.TestCase):
    def test_GreeterMale(self):
        self.assertEqual(Greeter(name="Max Mustermann", gender="male").greet(), "Hallo, Herr Max Mustermann!")
    def test_GreeterFemale(self):
        self.assertEqual(Greeter(name="Erika Mustermann", gender="female").greet(), "Hallo, Frau Erika Mustermann!")
    def test_GreeterNoGender(self):
        self.assertEqual(Greeter(name="Alex Mustermann").greet(), "Hallo, Alex Mustermann!")
